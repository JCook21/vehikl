<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'is-felipe-alive'], function () {
    Route::post('/days', 'IsFelipeAliveController@days');
    Route::post('/check', 'IsFelipeAliveController@check');
    Route::post('/sms-response', 'IsFelipeAliveController@smsResponse');
});

Route::group(['prefix' => 'parroter'], function () {
    Route::post('/', 'ParroterController@routeRequest')->name('parroter.routeSlackRequest');
});

Route::group(['prefix' => 'mentorship'], function () {
    Route::group(['prefix' => 'mentor'], function () {
        Route::post('/add-skill', 'Mentorship\\MentorController@add')->name('mentor.skill.add');
        Route::post('/delete-skill', 'Mentorship\\MentorController@delete')->name('mentor.skill.delete');
        Route::post('/list', 'Mentorship\\MentorController@list')->name('mentor.list');
    });

    Route::group(['prefix' => 'mentee'], function () {
        Route::post('/add-skill', 'Mentorship\\MenteeController@add')->name('mentee.skill.add');
        Route::post('/delete-skill', 'Mentorship\\MenteeController@delete')->name('mentee.skill.delete');
        Route::post('/list', 'Mentorship\\MenteeController@list')->name('mentee.list');
    });
});
