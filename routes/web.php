<?php

Route::group(['prefix' => 'parroter'], function () {
    Route::get('/authenticate', 'ParroterController@authenticate')->name('parroter.oauth');
    Route::get('/oauth', 'ParroterController@oauthCallback')->name('parroter.oauth.callback');
});

Route::group(['prefix' => 'veetup'], function () {
    Route::get('/authenticate', 'VeetupController@authenticate')->name('veetup.oauth');
    Route::get('/oauth', 'VeetupController@oauthCallback')->name('veetup.oauth.callback');
});
