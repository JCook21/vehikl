<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateIfaDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $app = \App\SlackApp::where('name', 'is_felipe_alive')->firstOrFail();
        $app->data = ['last_incident' => \Carbon\Carbon::parse('2019-02-27')];
        $app->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $app = \App\SlackApp::where('name', 'is_felipe_alive')->firstOrFail();
        $app->data = ['last_incident' => \Carbon\Carbon::parse('2018-12-06')];
        $app->save();
    }
}
