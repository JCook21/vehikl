<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVeetupSlackApp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\SlackApp::create([
            'name' => 'veetup',
            'data' => []
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $app = \App\SlackApp::where('name', 'veetup')->first();
        if($app) {
            $app->forceDelete();
        }
    }
}
