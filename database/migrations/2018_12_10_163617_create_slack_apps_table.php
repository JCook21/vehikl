<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlackAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slack_apps', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->longText('data')->nullable(true);
            $table->timestamps();

            $table->unique('name');
        });

        \App\SlackApp::create([
            'name' => 'is_felipe_alive',
            'data' => [
                'last_incident' => \Carbon\Carbon::parse('2018-12-06')
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slack_apps');
    }
}
