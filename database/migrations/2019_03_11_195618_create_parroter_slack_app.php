<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParroterSlackApp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\SlackApp::create([
            'name' => 'parroter',
            'data' => []
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $app = \App\SlackApp::where('name', 'parroter')->first();
        if($app) {
            $app->forceDelete();
        }
    }
}
