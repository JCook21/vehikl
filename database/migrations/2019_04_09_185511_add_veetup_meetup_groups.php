<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVeetupMeetupGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $app = \App\SlackApp::where('name', 'veetup')->first();
        if($app) {
            $appData = $app->data;
            $appData['groups'] = [
                'Tech-Savvy-Chapter-One-KPL',
                'Dev-London',
                'Girl-Geek-Dinners-Waterloo-Region',
                'Technical-Support-Innovators-P2P',
                'uxwaterloo-p2p',
                'women-in-technologyWR',
                'UX-Book-Club-KW',
                'London-FreeCodeCamp-Meetup',
                'Coder-Scoop',
                'Ladies-that-UX-London-Ontario',
                'HackerNestMIS',
                'CoderCamp-Hamilton'
            ];
            $app->data = $appData;
            $app->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $app = \App\SlackApp::where('name', 'veetup')->first();
        if($app) {
            $appData = $app->data;
            unset($appData['groups']);
            $app->data = $appData;
            $app->save();
        }
    }
}
