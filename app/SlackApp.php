<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SlackApp extends Model
{
    /**
     * @var array
     */
    protected $casts = [
        'data' => 'array',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'data'
    ];
}
