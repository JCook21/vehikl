<?php

namespace App\Services\Slack;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class Slack
{
    /**
     * @const string
     */
    const MESSAGE_EPHEMERAL_ENDPOINT = 'https://slack.com/api/chat.postEphemeral';

    /**
     * @const string
     */
    const REACTION_ENDPOINT = 'https://slack.com/api/reactions.add';

    /**
     * @const string
     */
    const MESSAGE_ENDPOINT = 'https://slack.com/api/chat.postMessage';

    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $oauthToken;

    /**
     * @param Client $client
     * @param string $oauthToken
     * @return void
     */
    public function __construct(Client $client, string $oauthToken)
    {
        $this->client = $client;
        $this->oauthToken = $oauthToken;
    }

    /**
     * @param string $userId
     * @param string $channelId
     * @param string $message
     * @param array $attachments
     * @return ResponseInterface
     */
    public function postEphemeral(
        string $userId,
        string $channelId,
        string $message,
        array $attachments = []
    ): ResponseInterface
    {
        return $this->client->post(self::MESSAGE_EPHEMERAL_ENDPOINT, [
            'json'    => [
                'channel'     => $channelId,
                'user'        => $userId,
                'text'        => $message,
                'attachments' => json_encode($attachments)
            ],
            'headers' => $this->generateHeaders($this->oauthToken)
        ]);
    }

    /**
     * @param string $channelId
     * @param string $timestamp
     * @param string $emojiName
     * @param string $userOauthToken
     * @return ResponseInterface
     */
    public function postReaction(
        string $channelId,
        string $timestamp,
        string $emojiName,
        string $userOauthToken
    ): ResponseInterface
    {
        return $this->client->post(self::REACTION_ENDPOINT, [
            'json'    => [
                'channel'   => $channelId,
                'timestamp' => $timestamp,
                'name'      => $emojiName
            ],
            'headers' => $this->generateHeaders($userOauthToken)
        ]);
    }

    /**
     * @param string $channelName
     * @param string $message
     * @return ResponseInterface
     */
    public function postMessage(string $channelName, string $message): ResponseInterface
    {
        return $this->client->post(self::MESSAGE_ENDPOINT, [
            'json' => [
                'channel' => $channelName,
                'message' => $message,
                'link_names' => true,
            ],
            'headers' => $this->generateHeaders($this->oauthToken)
        ]);
    }

    /**
     * @param string $authToken
     * @return array
     */
    private function generateHeaders(string $authToken): array
    {
        return [
            'Authorization' => 'Bearer ' . $authToken,
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        ];
    }
}
