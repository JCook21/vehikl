<?php

namespace App\Services;

use App\SlackApp;
use Nexmo\Client as NexmoClient;
use Nexmo\Message\Message as NexmoMessage;
use Carbon\Carbon;

class IsFelipeAliveService
{
    /**
     * @var SlackApp
     */
    protected $slackApp;

    /**
     * @var NexmoClient
     */
    protected $nexmo;

    /**
     * @var array
     */
    protected static $messages = [
        'check' => 'Hello, Felipe. Are you alive? Respond \'YES\' if you are.',
        'alive' => '<!here> Felipe has responded to the text message. He is ALIVE!',
        'dead'  => '<!here> Felipe has failed to respond to the text message in time. The only reasonable conclusion is that he is dead.'
    ];

    /**
     * @return void
     */
    public function __construct()
    {
        $this->slackApp = SlackApp::where('name', 'is_felipe_alive')->first();
        $this->nexmo = resolve('Nexmo\Client');
    }

    /**
     * @return int|null
     */
    public function getDaysSinceLastIncident() : ?int
    {
        if(!$this->slackApp) {
            return null;
        }

        $lastIncident = Carbon::parse(
            $this->slackApp->data['last_incident']['date'],
            $this->slackApp->data['last_incident']['timezone']
        );
        $now = now($this->slackApp->data['last_incident']['timezone']);
        return $lastIncident->diffInDays($now);
    }

    /**
     * @return bool
     */
    public function isOpenRequest() : bool
    {
        if(!$this->slackApp) {
            return false;
        }

        $request = $this->slackApp->data['requests'][0];

        $requestDate = Carbon::parse(
            $request['created_at']['date'],
            $request['created_at']['timezone']
        );

        $now = now($this->slackApp->data['last_incident']['timezone']);

        return !$request['responded']
            && $requestDate->diffInMinutes($now) < 10;
    }

    /**
     * @param string $responseUrl
     * @return NexmoMessage|null
     * @throws NexmoClient\Exception\Exception
     * @throws NexmoClient\Exception\Request
     * @throws NexmoClient\Exception\Server
     */
    public function checkIfAlive(string $responseUrl) : ?NexmoMessage
    {
        if(!$this->slackApp || !$this->nexmo) {
            return null;
        }

        $this->updateLastIncidentDate();
        $this->storeRequest($responseUrl);
        return $this->sendSms(self::$messages['check']);
    }

    /**
     * @return bool
     */
    public function checkIfDead() : bool
    {
        if(!$this->slackApp) {
            return false;
        }

        $request = $this->slackApp->data['requests'][0];

        $requestDate = \Carbon\Carbon::parse(
            $request['created_at']['date'],
            $request['created_at']['timezone']
        );

        $now = now($this->slackApp->data['last_incident']['timezone']);

        return !$request['responded']
            && $requestDate->diffInMinutes($now) >= 10;
    }

    /**
     * @param null|string $date
     * @return void
     */
    protected function updateLastIncidentDate(?string $date = null) : void
    {
        $data = $this->slackApp->data;
        $data['last_incident'] = $date ? Carbon::parse($date) : Carbon::now();
        $this->slackApp->data = $data;
        $this->slackApp->save();
    }

    /**
     * @param string $responseUrl
     * @return void
     */
    protected function storeRequest(string $responseUrl) : void
    {
        $data = $this->slackApp->data;
        $data['requests'] = collect($data['requests'] ?? [])
            ->push([
                'created_at'   => Carbon::now()->jsonSerialize(),
                'response_url' => $responseUrl,
                'responded'    => false
            ])
            ->sortByDesc(function (array $request) {
                return $request['created_at']['date'];
            })
            ->values()
            ->toArray();

        $this->slackApp->data = $data;
        $this->slackApp->save();
    }

    /**
     * @param string $text
     * @return NexmoMessage
     * @throws NexmoClient\Exception\Exception
     * @throws NexmoClient\Exception\Request
     * @throws NexmoClient\Exception\Server
     */
    protected function sendSms(string $text) : NexmoMessage
    {
        return $this->nexmo->message()->send([
            'from' => 14373704579,
            'to' => 12269894750,
            'text' => $text
        ]);
    }

    /**
     * @return void
     */
    public function informFelipeIsAlive() : void
    {
        if(!$this->slackApp) {
            return;
        }

        $data = $this->slackApp->data;
        $data['requests'][0]['responded'] = true;
        $this->slackApp->data = $data;
        $this->slackApp->save();

        $this->sendDelayedSlackResponse(
            self::$messages['alive'],
            $this->slackApp->data['requests'][0]['response_url']
        );
    }

    /**
     * @return void
     */
    public function informFelipeIsDead() : void
    {
        if(!$this->slackApp) {
            return;
        }

        $data = $this->slackApp->data;
        $data['requests'][0]['responded'] = true;
        $this->slackApp->data = $data;
        $this->slackApp->save();

        $this->sendDelayedSlackResponse(
            self::$messages['dead'],
            $this->slackApp->data['requests'][0]['response_url']
        );
    }

    /**
     * @param $text
     * @param $responseUrl
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function sendDelayedSlackResponse($text, $responseUrl) : \Psr\Http\Message\ResponseInterface
    {
        /** @var \GuzzleHttp\Client $guzzle */
        $guzzle = resolve('GuzzleHttp\Client');

        return $guzzle->post($responseUrl, [
            'json' => [
                'response_type' => 'in_channel',
                'text'          => $text
            ]
        ]);
    }
}
