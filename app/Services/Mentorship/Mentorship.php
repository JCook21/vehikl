<?php

namespace App\Services\Mentorship;

use App\Entities\Mentorship\Skill;
use App\Entities\Mentorship\User;
use App\Services\Slack\Slack;

class Mentorship
{
    const MENTORSHIP_CHANNEL = 'mentorship';
    /**
     * @var Slack
     */
    private $slack;

    /**
     * @param Slack $slack
     * @return void
     */
    public function __construct(Slack $slack)
    {
        $this->slack = $slack;
    }

    /**
     * @param string $userId
     * @param string $userName
     * @param string $channelId
     * @param string $skill
     * @return void
     */
    public function addSkillToMentor(string $userId, string $userName, string $channelId, string $skill): void
    {
        /** @var User $slackUser */
        $slackUser = User::firstOrCreate(['id' => $userId], ['name' => $userName]);
        $slackUser->skills()->attach( Skill::firstOrCreate(['name' => $skill]), ['type' => 'mentor']);

        $this->slack->postEphemeral($userId, $channelId, "You have added *$skill* to your list of skills.");
        $message = "$userName has added the skill *$skill* that they would be willing to offer mentorship in";
        $this->slack->postMessage(self::MENTORSHIP_CHANNEL, $message);
    }

    /**
     * @param string $userId
     * @param string $userName
     * @param string $channelId
     * @param string $skill
     * @return void
     */
    public function removeSkillFromMentor(string $userId, string $userName, string $channelId, string $skill): void
    {
        /** @var User $slackUser */
        $slackUser = User::firstOrCreate(['id' => $userId], ['name' => $userName]);
        $slackUser->skills()
            ->wherePivot('type', '=', 'mentor')
            ->detach(Skill::firstOrCreate(['name' => $skill]));

        $this->slack->postEphemeral($userId, $channelId, "You have removed *$skill* from your list of skills.");
        $message = "$userName has removed the skill *$skill* from the skills they can offer mentorship in";
        $this->slack->postMessage(self::MENTORSHIP_CHANNEL, $message);
    }

    /**
     * @param string $userId
     * @param string $channelId
     * @return void
     */
    public function listMentors(string $userId, string $channelId): void
    {
        $messageContent = User::mentor()->get()
            ->map(function (User $user) {
                return '<@' . $user->name . '>'
                    . ' is willing to provide mentoring in the following topics: *'
                    . $user->mentorSkills->implode('name', ', ')
                    . '*';
            })
            ->implode("\n");

        if(empty($messageContent)) {
            $messageContent = 'No mentors have added any skills yet.';
        }

        $this->slack->postEphemeral($userId, $channelId, $messageContent);
    }

    /**
     * @param string $userId
     * @param string $userName
     * @param string $channelId
     * @param string $skill
     * @return void
     */
    public function addSkillToMentee(string $userId, string $userName, string $channelId, string $skill): void
    {
        /** @var User $slackUser */
        $slackUser = User::firstOrCreate(['id' => $userId], ['name' => $userName]);
        $slackUser->skills()->attach( Skill::firstOrCreate(['name' => $skill]), ['type' => 'mentee']);

        $this->slack->postEphemeral(
            $userId,
            $channelId,
            "You have added *$skill* to your list of desired skills."
        );
        $message = "$userName has added the skill *$skill* that they would like mentorship in";
        $this->slack->postMessage(self::MENTORSHIP_CHANNEL, $message);
    }

    /**
     * @param string $userId
     * @param string $userName
     * @param string $channelId
     * @param string $skill
     * @return void
     */
    public function removeSkillFromMentee(string $userId, string $userName, string $channelId, string $skill): void
    {
        /** @var User $slackUser */
        $slackUser = User::firstOrCreate(['id' => $userId], ['name' => $userName]);
        $slackUser->skills()
            ->wherePivot('type', '=', 'mentee')
            ->detach(Skill::firstOrCreate(['name' => $skill]));

        $this->slack->postEphemeral(
            $userId,
            $channelId,
            "You have removed *$skill* from your list of desired skills."
        );
        $message = "$userName has removed the skill *$skill* from the skills they would like mentorship in";
        $this->slack->postMessage(self::MENTORSHIP_CHANNEL, $message);
    }

    /**
     * @param string $userId
     * @param string $channelId
     * @return void
     */
    public function listMentees(string $userId, string $channelId): void
    {
        $messageContent = User::mentee()->get()
            ->map(function (User $user) {
                return '<@' . $user->name . '>'
                    . ' is looking for mentoring in the following topics: *'
                    . $user->menteeSkills->implode('name', ', ')
                    . '*';
            })
            ->implode("\n");

        if(empty($messageContent)) {
            $messageContent = 'No mentees have added any desired skills yet.';
        }

        $this->slack->postEphemeral($userId, $channelId, $messageContent);
    }
}
