<?php

namespace App\Services;

use App\SlackApp;
use GuzzleHttp\Client as GuzzleClient;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class VeetupService
{
    /**
     * @var SlackApp
     */
    protected $slackApp;

    /**
     * @var GuzzleClient;
     */
    protected $guzzle;

    /**
     * @return void
     */
    public function __construct()
    {
        $this->slackApp = SlackApp::where('name', 'veetup')->firstOrFail();
        $this->guzzle = resolve('GuzzleHttp\Client');
    }

    /**
     * @return void
     */
    public function configureOauth(): void
    {
        config(['services.meetup.client_id' => config('services.meetup.veetup.client_id')]);
        config(['services.meetup.client_secret' => config('services.meetup.veetup.client_secret')]);
        config(['services.meetup.redirect' => config('services.meetup.veetup.redirect')]);
    }

    /**
     * @param array $tokenInfo
     * @return bool
     */
    public function saveToken(array $tokenInfo): bool
    {
        $appData = $this->slackApp->data;
        $appData['token'] = $tokenInfo;
        $this->slackApp->data = $appData;
        return $this->slackApp->save();
    }

    /**
     * @return void
     */
    public function postUpcomingEvents(): void
    {
        $events = $this->getAllUpcomingEvents();
        if($events->isNotEmpty()) {
            $this->sendSlackNotifications(
                'There are ' . $events->count() . ' events happening next week:',
                $events
            );
        }
    }

    /**
     * @param string $message
     * @param Collection $events
     * @return void
     */
    protected function sendSlackNotifications(string $message, Collection $events): void
    {
        $blocks = [
            [
                'type' => 'section',
                'text' => [
                    'type' => 'mrkdwn',
                    'text' => '*' . $message . '*'
                ]
            ], [
                'type' => 'divider'
            ]
        ];

        foreach($events->toArray() as $event) {
            $section = [
                'type' => 'section',
                'text' => [
                    'type' => 'mrkdwn',
                    'text' => '*'
                        . $event['group']
                        . ' - '
                        . $event['name']
                        . "*\n*Location:* "
                        . $event['venue']
                        . "\n*Date and Time:* "
                        . Carbon::createFromFormat('Y-m-d H:i:s', $event['date'], 'EST')
                            ->format('M j, Y H:i')
                        . "\n<"
                        . $event['url']
                        . '|Find Event Page Here>'
                ]
            ];

            if($event['image']) {
                $section['accessory'] = [
                    'type' => 'image',
                    'image_url' => $event['image'],
                    'alt_text' => $event['name']
                ];
            }

            $blocks[] = $section;
            $blocks[] = ['type' => 'divider'];
        }

        $blocks[] = [
            'type' => 'section',
            'text' => [
                'type' => 'mrkdwn',
                'text' => "\n\n"
            ]
        ];

        $this->guzzle->post(config('services.slack.veetup.webhook'), [
            'json' => [
                'blocks' => json_encode($blocks)
            ],
            'headers' => [
                'Authorization' => 'Bearer ' . $this->getAccessToken(),
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ]
        ]);
    }

    /**
     * @return Collection
     */
    protected function getAllUpcomingEvents(): Collection
    {
        $events = [];
        foreach ($this->slackApp->data['groups'] as $group) {
            $events[] = $this->getGroupUpcomingEvents($group);
        }

        return collect(array_merge(...$events))
            ->filter(function ($event) {
                $eventDay = Carbon::createFromFormat('Y-m-d', $event['local_date'], 'EST');
                $nextWeek = now('EST')->addWeek();
                return $nextWeek->format('W') === $eventDay->format('W');
            })
            ->map(function ($event) {
                return [
                    'name'      => $event['name'],
                    'date'      => $event['local_date'] . ' ' . $event['local_time'] . ':00',
                    'url'       => $event['link'],
                    'group'     => $event['group']['name'],
                    'image'     => $event['featured_photo']['thumb_link'] ?? null,
                    'venue'     => implode(', ', array_filter([
                        'name'        => $event['venue']['name'] ?? null,
                        'address_1'   => $event['venue']['address_1'] ?? null,
                        'address_2'   => $event['venue']['address_2'] ?? null,
                        'city'        => $event['venue']['city'] ?? null,
                        'postal_code' => $event['venue']['zip'] ?? null
                    ]))
                ];
            })
            ->sortBy('date');
    }

    /**
     * @param string $groupName
     * @return array
     */
    protected function getGroupUpcomingEvents(string $groupName): array
    {
        $response = $this->guzzle->get('https://api.meetup.com/' . $groupName . '/events', [
            'query'   => ['page' => 200, 'status' => 'upcoming', 'fields' => 'featured_photo'],
            'headers' => ['Authorization' => 'Bearer ' . $this->getAccessToken()]
        ]);

        $response = $response->getBody()->getContents();
        \Log::info($response);
        return json_decode($response, true);
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->slackApp->data['token']['access_token'];
    }
}
