<?php

namespace App\Services;

use App\Services\Slack\Slack;
use App\SlackApp;
use GuzzleHttp\Client as GuzzleClient;
use JoliCode\Slack\Api\Client as SlackClient;

class ParroterService
{
    /**
     * @var SlackApp
     */
    protected $slackApp;

    /**
     * @var SlackClient
     */
    protected $slackApiApp;

    /**
     * @var GuzzleClient;
     */
    protected $guzzle;

    /**
     * @var Slack
     */
    private $slack;

    /**
     * @param Slack $slack
     * @return void
     */
    public function __construct(Slack $slack)
    {
        $this->slackApp = SlackApp::where('name', 'parroter')->firstOrFail();
        $this->slackApiApp = app()->makeWith(
            'JoliCode\Slack\ClientFactory',
            ['token' => config('services.slack.parroter.oauth_token')]
        );
        $this->guzzle = resolve('GuzzleHttp\Client');
        $this->slack = $slack;
    }

    /**
     * @return void
     */
    public function configureOauth() : void
    {
        config(['services.slack.client_id'     => config('services.slack.parroter.client_id')]);
        config(['services.slack.client_secret' => config('services.slack.parroter.client_secret')]);
        config(['services.slack.redirect'      => config('services.slack.parroter.redirect')]);
    }

    /**
     * @param string $userId
     * @param string $token
     * @return bool
     */
    public function saveUserToken(string $userId, string $token) : bool
    {
        $appData = $this->slackApp->data;
        if(!isset($appData['tokens'])) {
            $appData['tokens'] = [];
        }

        $appData['tokens'][$userId] = $token;
        $this->slackApp->data = $appData;
        return $this->slackApp->save();
    }

    /**
     * @param string $userId
     * @return bool
     */
    public function userTokenExists(string $userId) : bool
    {
        return !empty($this->slackApp->data['tokens'][$userId]);
    }

    /**
     * @param string $userId
     * @return null|string
     */
    public function getUserToken(string $userId) : ?string
    {
        return $this->slackApp->data['tokens'][$userId] ?? null;
    }

    /**
     * @param string $channelId
     * @param string $userId
     * @return bool
     */
    public function requestAuthentication(string $channelId, string $userId) : bool
    {
        $this->slack->postEphemeral(
            $userId,
            $channelId,
            'You must authenticate with Parroter before you can use to post reactions.',
            [
                [
                    'title' => 'Authentication Required',
                    'fallback' => 'Sign in here: ' . route('parroter.oauth'),
                    'actions' => [
                        [
                            'type' => 'button',
                            'text' => 'Sign In Here',
                            'url'  => route('parroter.oauth')
                        ]
                    ]
                ]
            ]
        );

        return true;
    }

    /**
     * @return array
     */
    public function getAllParrots() : array
    {
        return array_keys(array_filter(
            (array)$this->slackApiApp->emojiList()['emoji'],
            function ($emojiName) {
                return strpos($emojiName, 'parrot') > -1;
            },
            ARRAY_FILTER_USE_KEY
        ));
    }

    /**
     * @param string $token
     * @param string $channel
     * @param string $timestamp
     * @return bool
     */
    public function postReactions(string $token, string $channel, string $timestamp) : bool
    {
        foreach($this->getAllParrots() as $emoji) {
            $this->slack->postReaction($channel, $timestamp, $emoji, $token);
        }

        return true;
    }
}
