<?php

namespace App\Http\Controllers;

use Illuminate\Http\{ RedirectResponse, Request };
use App\Services\ParroterService;
use Laravel\Socialite\Facades\Socialite;
use Mpociot\Socialite\Slack\Provider;

class ParroterController extends Controller
{
    /**
     * @param ParroterService $parroter
     * @return RedirectResponse
     */
    public function authenticate(ParroterService $parroter)
    {
        info('authenticate');
        info('-----------------');
        $parroter->configureOauth();

        return Socialite::with('slack')->scopes([
            'reactions:write'
        ])->redirect();
    }

    /**
     * @param ParroterService $parroter
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function oauthCallback(ParroterService $parroter, Request $request)
    {
        info('oauthCallback');
        info(json_encode($request->all()));
        info('-----------------');

        $parroter->configureOauth();

        /** @var Provider $slack */
        $slack = Socialite::driver('slack')->stateless();
        $token = $slack->getAccessToken($request->input('code'));
        $id = $slack->userFromToken($token)->getId();
        $parroter->saveUserToken($id, $token);

        return view('success');
    }

    /**
     * @var array
     */
    protected $callbackIdRouting = [
        'all-the-parrots' => 'allTheParrots'
    ];

    /**
     * Slack Menu Options can only hit one URL for a given Slack app, so every menu option will hit this endpoint.
     * Use it to route the request to other controller methods based on the callback.
     *
     * @param ParroterService $parroter
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function routeRequest(ParroterService $parroter, Request $request)
    {
        $payload = json_decode($request->input('payload'));

        // send the user a message asking them to authenticate with the app
        if(!$parroter->userTokenExists($payload->user->id)) {
            return $this->requestAuthentication($parroter, $request);
        }

        // perform the correct callback
        if(array_key_exists($payload->callback_id, $this->callbackIdRouting)) {
            return $this->{$this->callbackIdRouting[$payload->callback_id]}($parroter, $request);
        }

        // callback not found
        return response()->json('', 404);
    }

    /**
     * @param ParroterService $parroter
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function requestAuthentication(ParroterService $parroter, Request $request)
    {
        $payload = json_decode($request->input('payload'));
        $parroter->requestAuthentication($payload->channel->id, $payload->user->id);
        return response()->json('', 200);
    }

    /**
     * @param ParroterService $parroter
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    protected function allTheParrots(ParroterService $parroter, Request $request)
    {
        $payload = json_decode($request->input('payload'));
        if(
            $payload->type !== 'message_action'
            || $payload->message->type === 'ephemeral'
        ) {
            return response()->json('', 200);
        }

        $parroter->postReactions(
            $parroter->getUserToken($payload->user->id),
            $payload->channel->id,
            $payload->message->ts
        );

        return response()->json('', 200);
    }
}
