<?php

namespace App\Http\Controllers;

use App\Services\IsFelipeAliveService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client as GuzzleClient;

class IsFelipeAliveController extends Controller
{
    /**
     * @param IsFelipeAliveService $ifa
     * @return \Illuminate\Http\JsonResponse
     */
    public function days(IsFelipeAliveService $ifa)
    {
        return response()->json([
            'response_type' => 'in_channel',
            'text' => 'It has been '
                . $ifa->getDaysSinceLastIncident()
                . ' days since the last time we wondered if Felipe was alive.'
        ]);
    }

    /**
     * @param Request $request
     * @param IsFelipeAliveService $ifa
     * @return \Illuminate\Http\JsonResponse
     */
    public function check(Request $request, IsFelipeAliveService $ifa)
    {
        if($ifa->isOpenRequest()) {
            return response()->json([
                'response_type' => 'ephemeral',
                'text' => 'Someone has already recently triggered a text message to Felipe.'
            ]);
        }

        try {
            $ifa->checkIfAlive($request->input('response_url'));
        }
        catch(\Exception $e) {

            return response()->json([
                'response_type' => 'ephemeral',
                'text' => $e->getMessage()
            ]);
        }

        return response()->json([
            'response_type' => 'in_channel',
            'text' => 'A text message has been sent to Felipe. '
                . 'He has ten minutes to respond and confirm that he is alive.'
        ]);
    }

    /**
     * @param Request $request
     * @param IsFelipeAliveService $ifa
     * @return \Illuminate\Http\JsonResponse
     */
    public function smsResponse(Request $request, IsFelipeAliveService $ifa)
    {
        $this->logSmsResponse($request);

        if($ifa->isOpenRequest()) {
            if(strtoupper(trim($request->input('text'))) === 'YES') {
                $ifa->informFelipeIsAlive();
            }
        }

        return response()->json();
    }

    /**
     * @param Request $request
     * @return void
     */
    protected function logSmsResponse(Request $request) : void
    {
        Log::info('Received SMS response from '
            . ($request->input('msisdn') ?? '--NUMBER NOT FOUND--')
            . ':');
        Log::info(json_encode($request->all()));
    }
}
