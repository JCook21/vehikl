<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use App\Services\VeetupService;
use Laravel\Socialite\Facades\Socialite;
use Mpociot\Socialite\Slack\Provider;

class VeetupController extends Controller
{
    /**
     * @param VeetupService $veetup
     * @return RedirectResponse
     */
    public function authenticate(VeetupService $veetup)
    {
        $veetup->configureOauth();

        return Socialite::with('meetup')->scopes([
            'ageless', 'basic'
        ])->redirect();
    }

    /**
     * @param VeetupService $veetup
     * @return \Illuminate\View\View
     */
    public function oauthCallback(VeetupService $veetup)
    {
        $veetup->configureOauth();

        /** @var Provider $meetup */
        $meetup = Socialite::driver('meetup')->stateless();
        $veetup->saveToken($meetup->user()->accessTokenResponseBody);

        return view('success');
    }
}
