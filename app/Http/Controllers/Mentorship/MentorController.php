<?php

namespace App\Http\Controllers\Mentorship;

use App\Http\Controllers\Controller;
use App\Services\Mentorship\Mentorship;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MentorController extends Controller
{
    private $mentorShip;

    /**
     * MentorController constructor.
     * @param $mentorShip
     */
    public function __construct(Mentorship $mentorShip)
    {
        $this->mentorShip = $mentorShip;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function add(Request $request)
    {
        $this->mentorShip->addSkillToMentor(
            $request->input('user_id'),
            $request->input('user_name'),
            $request->input('channel_id'),
            $request->input('text')
        );

        return response('', 200);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function delete(Request $request)
    {
        $this->mentorShip->removeSkillFromMentor(
            $request->input('user_id'),
            $request->input('user_name'),
            $request->input('channel_id'),
            $request->input('text')
        );

        return response('', 200);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function list(Request $request)
    {
        $this->mentorShip->listMentors($request->input('user_id'), $request->input('channel_id'));

        return response('', 200);
    }
}
