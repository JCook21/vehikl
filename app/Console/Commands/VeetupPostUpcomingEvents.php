<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\VeetupService;

class VeetupPostUpcomingEvents extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'veetup:post';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Post upcoming events to Slack.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        resolve(VeetupService::class)
            ->postUpcomingEvents();
    }
}
