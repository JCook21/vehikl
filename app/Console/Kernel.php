<?php

namespace App\Console;

use App\Console\Commands\VeetupPostUpcomingEvents;
use App\Services\IsFelipeAliveService;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        VeetupPostUpcomingEvents::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            /** @var IsFelipeAliveService $ifa */
            $ifa = resolve('App\Services\IsFelipeAliveService');
            if($ifa->checkIfDead()) {
                $ifa->informFelipeIsDead();
            }
        })->everyMinute();

        $schedule->job(new VeetupPostUpcomingEvents)
            ->timezone('EST')
            ->weeklyOn(5, '08:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
