<?php

namespace App\Providers;

use App\Services\IsFelipeAliveService;
use App\Services\Mentorship\Mentorship;
use App\Services\ParroterService;
use App\Services\Slack\Slack;
use App\Services\VeetupService;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use JoliCode\Slack\ClientFactory as SlackFactory;
use Nexmo\Client as NexmoClient;
use Nexmo\Client\Credentials\Basic as NexmoCredentialsBasic;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->bindParroter();

        $this->app->singleton('App\Services\IsFelipeAliveService', function () {
            return new IsFelipeAliveService;
        });

        $this->app->singleton('App\Services\VeetupService', function () {
            return new VeetupService;
        });

        $this->app->singleton('Nexmo\Client', function () {
            return new NexmoClient(new NexmoCredentialsBasic(
                config('services.nexmo.key'),
                config('services.nexmo.secret')
            ));
        });

        $this->app->bind('GuzzleHttp\Client', function () {
            return new Client;
        });

        $this->app->bind('JoliCode\Slack\ClientFactory', function ($app, $params) {
            return SlackFactory::create($params['token']);
        });

        $this->app->bind(Slack::class, function () {
            return new Slack($this->app->make(Client::class), config('services.slack.parroter.oauth_token'));
        });

        $this->app->when(Mentorship::class)
            ->needs(Slack::class)
            ->give(function () {
                return new Slack($this->app->make(Client::class), config('services.slack.mentorship.oauth_token'));
            });
    }

    /**
     * @return void
     */
    private function bindParroter(): void
    {
        $this->app->singleton(ParroterService::class, function () {
            return new ParroterService($this->app->make(Slack::class));
        });
    }
}
