<?php

namespace App\Entities\Mentorship;

use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class User
 * @property string $id
 * @property string $name
 * @property Collection $skills
 * @method static QueryBuilder mentor()
 * @method static QueryBuilder mentee()
 */
class User extends Model
{
    /**
     * @var string
     */
    protected $table = 'mentorship_users';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'name'
    ];

    /**
     * @return BelongsToMany
     */
    public function mentorSkills(): BelongsToMany
    {
        return $this->skills()->wherePivot('type', '=', 'mentor');
    }

    /**
     * @return BelongsToMany
     */
    public function menteeSkills(): BelongsToMany
    {
        return $this->skills()->wherePivot('type', '=', 'mentee');
    }

    /**
     * @return BelongsToMany
     */
    public function skills(): BelongsToMany
    {
        return $this->belongsToMany(
            Skill::class,
            'mentorship_users_x_mentorship_skills',
            'user_id',
            'skill_name'
        );
    }

    /**
     * @param QueryBuilder $query
     * @return QueryBuilder
     */
    public function scopeMentor(QueryBuilder $query) : QueryBuilder
    {
        return $query->whereHas('mentorSkills');
    }

    /**
     * @param QueryBuilder $query
     * @return QueryBuilder
     */
    public function scopeMentee(QueryBuilder $query) : QueryBuilder
    {
        return $query->whereHas('menteeSkills');
    }
}
