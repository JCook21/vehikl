<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Success</title>
        <style>

        </style>
    </head>

    <body>
        <div>
            <h1>Successfully authenticated!</h1>
            <h2>Now you can go back to Slack and do... whatever it is you were trying to do.</h2>
        </div>
    </body>
</html>
