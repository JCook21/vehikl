<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'nexmo' => [
        'key' => env('NEXMO_KEY'),
        'secret' => env('NEXMO_SECRET')
    ],

    'slack' => [
        'parroter' => [
            'oauth_token' => env('PARROTER_SLACK_OAUTH_KEY'),
            'client_id' => env('PARROTER_SLACK_KEY'),
            'client_secret' => env('PARROTER_SLACK_SECRET'),
            'redirect' => env('PARROTER_SLACK_REDIRECT_URI')
        ],
        'veetup' => [
            'webhook' => env('VEETUP_SLACK_WEBHOOK')
        ],
        'mentorship' => [
            'oauth_token' => env('MENTORSHIP_SLACK_OAUTH_KEY'),
        ]
    ],

    'meetup' => [
        'veetup' => [
            'client_id' => env('VEETUP_MEETUP_KEY'),
            'client_secret' => env('VEETUP_MEETUP_SECRET'),
            'redirect' => env('VEETUP_MEETING_REDIRECT_URI'),
        ]
    ]

];
