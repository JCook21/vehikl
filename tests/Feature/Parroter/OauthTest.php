<?php

namespace Tests\Feature\Parroter;

use Exception;

class OauthTest extends ParroterTestCase
{
    /**
     * @return void
     */
    public function testItRedirectsToSlackAuthenticationWithParroterClientInfo(): void
    {
        $response = $this->get(route('parroter.oauth'));

        $response->assertStatus(302);
        $this->assertRedirectUrlContains('https://slack.com/oauth/authorize', $response);
        $this->assertRedirectUrlContains('client_id=' . config('services.slack.parroter.client_id'), $response);
        $this->assertRedirectUrlContains(
            'redirect_uri=' . urlencode(config('services.slack.parroter.redirect')), $response
        );
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testItReturnsASuccessfulResponseIfTheUserIsSuccessfullyAuthenticated(): void
    {
        $this->mockSocialiteToReturnUserIdAndToken(random_int(100, 999), str_random(16));
        $response = $this->get(route('parroter.oauth.callback'));

        $response->assertOk();
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testItSavesASuccessfullyAuthenticatedUserToTheDatabase(): void
    {
        $id = random_int(100, 999);
        $token = str_random(16);
        $this->mockSocialiteToReturnUserIdAndToken($id, $token);
        $this->get(route('parroter.oauth.callback'));

        $tokens = $this->getSlackApp()->data['tokens'];
        $this->assertArrayHasKey($id, $tokens);
        $this->assertEquals($token, $tokens[$id]);
    }
}
