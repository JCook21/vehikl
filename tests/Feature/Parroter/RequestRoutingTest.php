<?php

namespace Tests\Feature\Parroter;

use Tests\Traits\MocksGuzzleHistory;

class RequestRoutingTest extends ParroterTestCase
{
    use MocksGuzzleHistory;

    /**
     * @return void
     */
    public function testItReturnsSuccessForAllTheParrotsRoute(): void
    {
        $response = $this->post(
            route('parroter.routeSlackRequest'),
            $this->loadJsonFixture('slack_all_the_parrots.json')
        );

        $response->assertOk();
    }

    /**
     * @return void
     */
    public function testItReturnsNotFoundForAnUndefinedRoute(): void
    {
        $response = $this->post(
            'api/parroter/this-route-definitely-does-not-exist-trololol',
            $this->loadJsonFixture('slack_all_the_parrots.json')
        );

        $response->assertNotFound();
    }
}
