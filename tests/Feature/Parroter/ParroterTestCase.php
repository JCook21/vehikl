<?php

namespace Tests\Feature\Parroter;

use App\SlackApp;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

abstract class ParroterTestCase extends TestCase
{
    use RefreshDatabase;

    /**
     * @return SlackApp
     */
    protected function getSlackApp(): SlackApp
    {
        return SlackApp::where('name', 'parroter')->firstOrFail();
    }
}
