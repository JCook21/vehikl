<?php

namespace Tests\Feature\Mentorship;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Traits\MocksGuzzleHistory;

abstract class MentorshipTestCase extends TestCase
{
    use RefreshDatabase, MocksGuzzleHistory;

    /**
     * @return Collection
     */
    protected function provideUsersWithSkills(): Collection
    {
        return collect([
            [
                'id'     => '111',
                'name'   => 'geralt',
                'skills' => collect(['swordsmanship', 'alchemy', 'riding', 'signs'])
            ],
            [
                'id'     => '2222',
                'name'   => 'yennefer',
                'skills' => collect(['sorcery', 'teleporation'])
            ],
            [
                'id'     => '33333',
                'name'   => 'triss',
                'skills' => collect(['sorcery', 'fire'])
            ],
            [
                'id'     => '44444444',
                'name'   => 'cirilla',
                'skills' => collect(['swordsmanship', 'translocation'])
            ],
        ]);
    }
}
