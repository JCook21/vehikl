<?php

namespace Tests\Feature\Mentorship;

use App\Entities\Mentorship\Skill;
use App\Entities\Mentorship\User;

class AddMenteeSkillTest extends MentorshipTestCase
{
    /**
     * @return void
     */
    public function testItReturnsSuccessFromAValidSlackRequest(): void
    {
        $response = $this->post(route('mentee.skill.add'), $this->loadJsonFixture('slack_mentee_add_skill.json'));
        $response->assertOk();
    }

    /**
     * @return void
     */
    public function testItCreatesTheSlackUserIfTheyDoNotAlreadyExist(): void
    {
        $slackRequest = $this->loadJsonFixture('slack_mentee_add_skill.json');
        $this->assertNull(User::find($slackRequest['user_id']));
        $this->post(route('mentee.skill.add'), $slackRequest);

        $this->assertNotNull(User::find($slackRequest['user_id']));
    }

    /**
     * @return void
     */
    public function testItCreatesTheSkillIfItDoesNotAlreadyExist(): void
    {
        $slackRequest = $this->loadJsonFixture('slack_mentee_add_skill.json');
        $this->assertNull(Skill::find($slackRequest['text']));
        $this->post(route('mentee.skill.add'), $slackRequest);

        $this->assertNotNull(Skill::find($slackRequest['text']));
    }

    /**
     * @return void
     */
    public function testItAssociatesTheSkillWithTheMentee(): void
    {
        $slackRequest = $this->loadJsonFixture('slack_mentee_add_skill.json');
        $this->post(route('mentee.skill.add'), $slackRequest);

        $this->assertContains(
            $slackRequest['text'],
            User::find($slackRequest['user_id'])->menteeSkills->pluck('name')
        );
    }

    /**
     * @return void
     */
    public function testItNotifiesTheUserThatTheyHaveAddedTheSkill(): void
    {
        $slackRequest = $this->loadJsonFixture('slack_mentee_add_skill.json');
        $this->post(route('mentee.skill.add'), $slackRequest);

        $this->assertContains(
            "You have added *{$slackRequest['text']}* to your list of desired skills.",
            (string) $this->getGuzzleHistory()[0]['request']->getBody()
        );
    }
}
