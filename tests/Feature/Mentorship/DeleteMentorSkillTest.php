<?php

namespace Tests\Feature\Mentorship;

use App\Entities\Mentorship\Skill;
use App\Entities\Mentorship\User;

class DeleteMentorSkillTest extends MentorshipTestCase
{
    /**
     * @return void
     */
    public function testItReturnsSuccessFromAValidSlackRequest(): void
    {
        $response = $this->post(route('mentor.skill.delete'), $this->loadJsonFixture('slack_mentor_delete_skill.json'));
        $response->assertOk();
    }

    /**
     * @return void
     */
    public function testItCreatesTheSlackUserIfTheyDoNotAlreadyExist(): void
    {
        $slackRequest = $this->loadJsonFixture('slack_mentor_delete_skill.json');
        $this->assertNull(User::find($slackRequest['user_id']));
        $this->post(route('mentor.skill.delete'), $slackRequest);

        $this->assertNotNull(User::find($slackRequest['user_id']));
    }

    /**
     * @return void
     */
    public function testItCreatesTheSkillIfItDoesNotAlreadyExist(): void
    {
        $slackRequest = $this->loadJsonFixture('slack_mentor_delete_skill.json');
        $this->assertNull(Skill::find($slackRequest['text']));
        $this->post(route('mentor.skill.delete'), $slackRequest);

        $this->assertNotNull(Skill::find($slackRequest['text']));
    }

    /**
     * @return void
     */
    public function testItDetachesTheSkillFromTheMentor(): void
    {
        $slackRequest = $this->loadJsonFixture('slack_mentor_delete_skill.json');

        User::create(['id' => $slackRequest['user_id'], 'name' => $slackRequest['user_name']])
            ->skills()
            ->attach(Skill::create(['name' => $slackRequest['text']]), ['type' => 'mentor']);

        $this->assertContains(
            $slackRequest['text'],
            User::find($slackRequest['user_id'])->mentorSkills->pluck('name')
        );

        $this->post(route('mentor.skill.delete'), $slackRequest);

        $this->assertNotContains(
            $slackRequest['text'],
            User::find($slackRequest['user_id'])->mentorSkills->pluck('name')
        );
    }

    /**
     * @return void
     */
    public function testItNotifiesTheUserThatTheyHaveDeletedTheSkill(): void
    {
        $slackRequest = $this->loadJsonFixture('slack_mentor_delete_skill.json');
        $this->post(route('mentor.skill.delete'), $slackRequest);

        $this->assertContains(
            "You have removed *{$slackRequest['text']}* from your list of skills.",
            (string) $this->getGuzzleHistory()[0]['request']->getBody()
        );
    }
}
