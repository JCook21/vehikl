<?php

namespace Tests\Feature\Mentorship;

use App\Entities\Mentorship\Skill;
use App\Entities\Mentorship\User;

class ListMenteesTest extends MentorshipTestCase
{
    /**
     * @return void
     */
    public function testItReturnsSuccessFromAValidSlackRequest(): void
    {
        $response = $this->post(route('mentee.list'), $this->loadJsonFixture('slack_list_mentees.json'));
        $response->assertOk();
    }

    /**
     * @return void
     */
    public function testItSendsTheUserAListOfMentors(): void
    {
        $mentees = $this->provideUsersWithSkills()->map(function (array $menteeInfo) {
           $mentee = User::create(array_only($menteeInfo, ['id', 'name']));
           $menteeInfo['skills']->each(function (string $skill) use ($mentee) {
               $mentee->skills()->attach(Skill::firstOrCreate(['name' => $skill]), ['type' => 'mentee']);
           });

           return $mentee->fresh();
        });

        $this->post(route('mentee.list'), $this->loadJsonFixture('slack_list_mentees.json'));

        $mentees->each(function (User $mentee) {
            $expectedString = '<@' . $mentee->name . '>'
                . ' is looking for mentoring in the following topics: *'
                . $mentee->menteeSkills->implode('name', ', ')
                . '*';

            $this->assertContains($expectedString, (string) $this->getGuzzleHistory()[0]['request']->getBody());
        });
    }
}
