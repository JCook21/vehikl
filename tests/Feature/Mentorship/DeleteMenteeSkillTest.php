<?php

namespace Tests\Feature\Mentorship;

use App\Entities\Mentorship\Skill;
use App\Entities\Mentorship\User;

class DeleteMenteeSkillTest extends MentorshipTestCase
{
    /**
     * @return void
     */
    public function testItReturnsSuccessFromAValidSlackRequest(): void
    {
        $response = $this->post(route('mentee.skill.delete'), $this->loadJsonFixture('slack_mentee_delete_skill.json'));
        $response->assertOk();
    }

    /**
     * @return void
     */
    public function testItCreatesTheSlackUserIfTheyDoNotAlreadyExist(): void
    {
        $slackRequest = $this->loadJsonFixture('slack_mentee_delete_skill.json');
        $this->assertNull(User::find($slackRequest['user_id']));
        $this->post(route('mentee.skill.delete'), $slackRequest);

        $this->assertNotNull(User::find($slackRequest['user_id']));
    }

    /**
     * @return void
     */
    public function testItCreatesTheSkillIfItDoesNotAlreadyExist(): void
    {
        $slackRequest = $this->loadJsonFixture('slack_mentee_delete_skill.json');
        $this->assertNull(Skill::find($slackRequest['text']));
        $this->post(route('mentee.skill.delete'), $slackRequest);

        $this->assertNotNull(Skill::find($slackRequest['text']));
    }

    /**
     * @return void
     */
    public function testItDetachesTheSkillFromTheMentee(): void
    {
        $slackRequest = $this->loadJsonFixture('slack_mentee_delete_skill.json');

        User::create(['id' => $slackRequest['user_id'], 'name' => $slackRequest['user_name']])
            ->skills()
            ->attach(Skill::create(['name' => $slackRequest['text']]), ['type' => 'mentee']);

        $this->assertContains(
            $slackRequest['text'],
            User::find($slackRequest['user_id'])->menteeSkills->pluck('name')
        );

        $this->post(route('mentee.skill.delete'), $slackRequest);

        $this->assertNotContains(
            $slackRequest['text'],
            User::find($slackRequest['user_id'])->menteeSkills->pluck('name')
        );
    }

    /**
     * @return void
     */
    public function testItNotifiesTheUserThatTheyHaveDeletedTheSkill(): void
    {
        $slackRequest = $this->loadJsonFixture('slack_mentee_delete_skill.json');
        $this->post(route('mentee.skill.delete'), $slackRequest);

        $this->assertContains(
            "You have removed *{$slackRequest['text']}* from your list of desired skills.",
            (string) $this->getGuzzleHistory()[0]['request']->getBody()
        );
    }
}
