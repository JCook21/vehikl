<?php

namespace Tests\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Response;

trait MocksGuzzleHistory
{
    /**
     * @var array
     */
    private $guzzleHistory;

    /**
     * @var MockHandler
     */
    private $mockHandler;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->guzzleHistory = [];
        $this->mockGuzzleHistory($this->guzzleHistory, [new Response(200), new Response(200)]);
    }

    /**
     * @param array $container
     * @param array $responses
     * @return array
     */
    protected function mockGuzzleHistory(array &$container, array $responses): array
    {
        $this->mockHandler = new MockHandler($responses);
        $history = Middleware::history($container);
        $stack = HandlerStack::create($this->mockHandler);
        $stack->push($history);
        $mockedGuzzle = new Client(['handler' => $stack]);
        $this->app->bind('GuzzleHttp\Client', function () use ($mockedGuzzle) {
            return $mockedGuzzle;
        });

        return $container;
    }

    protected function getGuzzleHistory(): array
    {
        return $this->guzzleHistory;
    }

    /**
     * @return void
     */
    protected function restoreGuzzle(): void
    {
        $this->app->bind('GuzzleHttp\Client', function () {
            return new Client;
        });
    }

    /**
     * @return void
     */
    public function tearDown(): void
    {
        $this->restoreGuzzle();
        parent::tearDown();
    }
}
