<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\TestResponse;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Two\User as Oauth2User;
use Mpociot\Socialite\Slack\Provider as Oauth2Provider;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * @param string $needle
     * @param TestResponse $response
     * @return void
     */
    protected function assertRedirectUrlContains(string $needle, TestResponse $response): void
    {
        $this->assertContains(
            $needle,
            $response->headers->get('location'),
            "Failed asserting that the response's redirect URL includes $needle."
        );
    }

    /**
     * @param int $id
     * @param string $token
     * @return void
     */
    protected function mockSocialiteToReturnUserIdAndToken(int $id, string $token): void
    {
        $user = $this->createMock(Oauth2User::class);
        $user->method('getId')->willReturn($id);

        $driver = $this->createMock(Oauth2Provider::class);
        $driver->method('stateless')->willReturn($driver);
        $driver->method('getAccessToken')->willReturn($token);
        $driver->method('userFromToken')->willReturn($user);

        Socialite::shouldReceive('driver')->andReturn($driver);
    }

    /**
     * @param string $filename
     * @return mixed[]
     */
    protected function loadJsonFixture(string $filename): array
    {
        return json_decode(file_get_contents(__DIR__ . '/fixtures/' . $filename), true);
    }
}
