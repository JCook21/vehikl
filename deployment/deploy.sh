#!/bin/sh

# REMEMBER: pass a deployment environment to this script as an argument! ie, `source deploy.sh STAGE`

# Remove the testing .env file
rm .env

# Create a new .env file
touch .env

# Loop through the CodeShip environment variables that match our environment namespace
# (ie ENV_<environment>_DB_CONNECTION)
# And add them to the .env file.
while IFS='=' read -r name value ; do
	if [[ $name == "ENV_$1_"* ]]; then
	    envname=${name#*_*_}
	    echo ${envname}"="${!name}>> .env
	fi
done < <(env)

# Remove any unneeded files that were generated from the testing step.
rm .php-version

# Zip up the application
zip -r ~/app.zip ./

# Define our deployment user and deployment path from our passed deployment environment.
# (ie REMOTE_SSH_USER_<environment>)
deploy_user="REMOTE_SSH_USER_$1"
deploy_user_password="REMOTE_SSH_USER_PASSWORD_$1"
deploy_path="REMOTE_SSH_DEPLOY_PATH_$1"

# Copy the Zip file to the server.
scp ~/app.zip ${!deploy_user}:${!deploy_path}

# SSH into the deployment server and change directory to the application directory.
# Unzip the application in that directory.
# Run the postdeploy.sh script.
# Remove the deployment directory.
# Then the SSH session.
ssh ${!deploy_user} -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o PreferredAuthentications=publickey '
	cd '${!deploy_path}';
	unzip -o app.zip -d ./;
	source deployment/postdeploy.sh '$1' '${!deploy_user_password}';
	rm -r deployment;
	exit;'