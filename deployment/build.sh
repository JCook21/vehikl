#!/bin/sh

# CodeShip uses phpenv to manage PHP versions.
# Set PHP version.
phpenv local 7.2

# CodeShip uses PECL to manage PHP extensions.
# Install any required PHP extensions via PECL.
#pecl install -f memcache

# Create a MySQL database on the CodeShip image.
# This will be used in the testing step.
mysql -e 'create database test_db;'
mysql -e 'CREATE USER "test_db"@"localhost" IDENTIFIED BY "secret";'
mysql -e 'GRANT ALL PRIVILEGES ON * . * TO "test_db"@"localhost";'

# Create a .env file for the testing step.
touch .env
echo "APP_ENV=testing">> .env
echo "APP_KEY=">> .env
echo "APP_DEBUG=true" >> .env
echo "DB_CONNECTION=mysql" >> .env
echo "DB_HOST=localhost" >> .env
echo "DB_DATABASE=test_db" >> .env
echo "DB_USERNAME=test_db">> .env
echo "DB_PASSWORD=secret" >> .env
echo "CACHE_DRIVER=array" >> .env
echo "SESSION_DRIVER=array" >> .env
echo "QUEUE_DRIVER=sync" >> .env

# Prepare Laravel cache directory.
mkdir -p ./bootstrap/cache

# Install composer dependencies.
composer install --no-interaction

# Generate application key
php artisan key:generate

# Run Laravel migrations on the test database.
# By adding the --force flag, we prevent any blocking dialogue.
php artisan migrate --force

# Force exit if the migrations fail.
if [ $? != 0 ]; then
    exit
fi
